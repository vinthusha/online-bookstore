-- MySQL dump 10.13  Distrib 5.7.12, for Win64 (x86_64)
--
-- Host: localhost    Database: bookshop
-- ------------------------------------------------------
-- Server version	5.7.15-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `addbook`
--

DROP TABLE IF EXISTS `addbook`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `addbook` (
  `BookID` int(11) DEFAULT NULL,
  `BookName` varchar(50) DEFAULT NULL,
  `Auther` varchar(45) DEFAULT NULL,
  `Category` varchar(30) DEFAULT NULL,
  `BookPrice` double DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `addbook`
--

LOCK TABLES `addbook` WRITE;
/*!40000 ALTER TABLE `addbook` DISABLE KEYS */;
INSERT INTO `addbook` VALUES (1,'PythonCrashCourse','EricMatthes','ProgrammingLanguage',850,97),(2,'EloquentJavaScript','MarijnHaverbeke','ProgrammingLanguage',1000,99),(3,'WebDesignWithHTMLandCS','JonDuckett','ProgrammingLanguage',950,100),(4,'PythonProgramming','MichaelDawson','ProgrammingLanguage',500,100),(5,'LearningPython','MarkLutz','ProgrammingLanguage',2000,98),(6,'EffictiveJava','JoshuaBloch','ProgrammingLanguage',1500,100),(7,'ThePilgrim\'sProgress','JohnBunyan','Novel',1850,100),(8,'RobinsonCrusoe','DanielDefoe','Novel',2500,100),(9,'Clarissa','SamuelRichardson','Novel',800,100),(10,'Emma','JaneAusten','Novel',1600,100),(11,'TomJones','HenryFielding','Novel',2100,100),(12,'HarryPotterAndHalf-BloodPrince','J.K.Rowling','Novel',3000,100),(13,'AnimalFarm','GeorgeOrwel','Novel',700,100),(14,'NewMoon','StephenieMeyer','Novel',300,100);
/*!40000 ALTER TABLE `addbook` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shoppingcart`
--

DROP TABLE IF EXISTS `shoppingcart`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shoppingcart` (
  `BId` int(11) NOT NULL AUTO_INCREMENT,
  `CustomerName` varchar(50) DEFAULT NULL,
  `BookName` varchar(50) DEFAULT NULL,
  `Auther` varchar(30) DEFAULT NULL,
  `Category` varchar(50) DEFAULT NULL,
  `Quantity` int(11) DEFAULT NULL,
  `TotalPrice` double DEFAULT NULL,
  PRIMARY KEY (`BId`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shoppingcart`
--

LOCK TABLES `shoppingcart` WRITE;
/*!40000 ALTER TABLE `shoppingcart` DISABLE KEYS */;
INSERT INTO `shoppingcart` VALUES (1,'mathunujan','PythonCrashCourse','EricMatthes','ProgrammingLanguage',2,1700),(2,'mathunujan','PythonCrashCourse','EricMatthes','ProgrammingLanguage',2,1700),(3,'logitha','PythonCrashCourse','EricMatthes','ProgrammingLanguage',2,1700),(4,'logitha','EloquentJavaScript','MarijnHaverbeke','ProgrammingLanguage',1,1000),(5,'vinthusha','LearningPython','MarkLutz','ProgrammingLanguage',1,2000),(6,'vinthusha','LearningPython','MarkLutz','ProgrammingLanguage',1,2000);
/*!40000 ALTER TABLE `shoppingcart` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `userid` int(11) DEFAULT NULL,
  `name` varchar(20) DEFAULT NULL,
  `password` varchar(30) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `address` varchar(20) DEFAULT NULL,
  `status` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'01','123','vinthusha','no','jaffna'),(1,'01','123','vinthusha','no','jaffna'),(1,'01','logi','Logitha','unmarried','Jaffna'),(1,'01','logi','Logitha','unmarried','Jaffna'),(3,'03','Shana','Shana','Unmarried','Kokkuvil'),(5,'05','555','thanogika','gdfgfdg','trinco'),(25,'25','mathu123','mathunujan','single','kokuvil');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `userid` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `password` varchar(30) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `address` varchar(50) DEFAULT NULL,
  `status` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`userid`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'mathunujan','mathu123','smathunujan@gmail.com','kokuvil','single'),(2,'vinthusha','thanoji','lvinthusha@gmail.com','trinco','single'),(3,'Logitha','logitha123','Logi@gmail.com','jaffna','single'),(4,'vinthusha','456','vinthusha@gmail.com','trinco','single'),(5,'vinthusha','678','vinthusha@gmail.com','jaffna','single');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-05-19  9:13:08
