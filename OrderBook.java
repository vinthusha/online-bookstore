package bcas.assi.bookstore;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

public class OrderBook extends JFrame {
	static int qty = 0;
	static int bookId = 0;
	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					OrderBook frame = new OrderBook();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public OrderBook() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBackground(Color.PINK);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblNewLabel = new JLabel("OrderBook");
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblNewLabel.setBounds(161, 21, 107, 29);
		contentPane.add(lblNewLabel);

		JLabel lblNewLabel_1 = new JLabel("Book ID");
		lblNewLabel_1.setForeground(Color.BLUE);
		lblNewLabel_1.setBounds(42, 75, 46, 14);
		contentPane.add(lblNewLabel_1);

		JLabel lblNewLabel_2 = new JLabel("Qty");
		lblNewLabel_2.setForeground(Color.BLUE);
		lblNewLabel_2.setBounds(42, 112, 46, 14);
		contentPane.add(lblNewLabel_2);

		textField = new JTextField();
		textField.setBounds(102, 72, 86, 20);
		contentPane.add(textField);
		textField.setColumns(10);

		textField_1 = new JTextField();
		textField_1.setBounds(102, 109, 86, 20);
		contentPane.add(textField_1);
		textField_1.setColumns(10);

		JButton btnNewButton = new JButton("Back");
		btnNewButton.setFont(new Font("Tahoma", Font.BOLD, 12));
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		btnNewButton.setForeground(Color.BLUE);
		btnNewButton.setBounds(43, 165, 67, 37);
		contentPane.add(btnNewButton);

		JButton btnNewButton_1 = new JButton("Add To Shoping Card");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				qty = Integer.parseInt(textField_1.getText());
				bookId = Integer.parseInt(textField.getText());
				if (ShopCardDB.checkBook(bookId)) {

					int i = ShopCardDB.save(bookId);
					if (i > 0) {
						JOptionPane.showMessageDialog(OrderBook.this, "Book added successfully!");
						ShoppingCard.main(new String[] {});
						// frame.dispose();

					} else {
						JOptionPane.showMessageDialog(OrderBook.this, "Sorry, unable to issue!");
					} // end of save if-else

				} else {
					JOptionPane.showMessageDialog(OrderBook.this, "Sorry, Callno doesn't exist!");
				} // end of checkbook if-else
			}

		});
		btnNewButton_1.setFont(new Font("Tahoma", Font.BOLD, 12));
		btnNewButton_1.setForeground(Color.BLUE);
		btnNewButton_1.setBounds(254, 75, 170, 37);
		contentPane.add(btnNewButton_1);

		JButton btnNewButton_2 = new JButton("Buy");
		btnNewButton_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				qty = Integer.parseInt(textField_1.getText());
				bookId = Integer.parseInt(textField.getText());
				if (qty > 0) {
					if (SearchDB.checkBook(bookId)) {
						ConfirmOrder.main(new String[] {});
						// frame.dispose();

					} else {
						JOptionPane.showMessageDialog(OrderBook.this, " Sorry, bookno doesn't exist!");
					} // end of checkbook if-else

				} else {
					JOptionPane.showMessageDialog(OrderBook.this, "Sorry, unable to issue!");
				} // end of bookId if-else
			}
		});

		btnNewButton_2.setFont(new Font("Tahoma", Font.BOLD, 12));
		btnNewButton_2.setForeground(Color.BLUE);
		btnNewButton_2.setBounds(313, 165, 67, 37);
		contentPane.add(btnNewButton_2);
	}
}
