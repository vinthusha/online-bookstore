package bcas.assi.bookstore;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

public class Bill extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	private JTextField textField_4;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Bill frame = new Bill();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Bill() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBackground(Color.PINK);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		textField = new JTextField();
		textField.setBounds(21, 80, 111, 20);
		contentPane.add(textField);
		textField.setColumns(10);

		textField_1 = new JTextField();
		textField_1.setBounds(183, 80, 123, 20);
		contentPane.add(textField_1);
		textField_1.setColumns(10);

		textField_2 = new JTextField();
		textField_2.setBounds(356, 80, 52, 20);
		contentPane.add(textField_2);
		textField_2.setColumns(10);

		textField_3 = new JTextField();
		textField_3.setBounds(35, 149, 86, 20);
		contentPane.add(textField_3);
		textField_3.setColumns(10);

		textField_4 = new JTextField();
		textField_4.setBounds(201, 149, 108, 20);
		contentPane.add(textField_4);
		textField_4.setColumns(10);

		JLabel lblNewLabel = new JLabel("Name");
		lblNewLabel.setBounds(53, 55, 46, 14);
		contentPane.add(lblNewLabel);

		JLabel lblNewLabel_1 = new JLabel("Book name");
		lblNewLabel_1.setBounds(201, 55, 65, 14);
		contentPane.add(lblNewLabel_1);

		JLabel lblNewLabel_2 = new JLabel("Book Qty");
		lblNewLabel_2.setBounds(362, 55, 46, 14);
		contentPane.add(lblNewLabel_2);

		JLabel lblNewLabel_3 = new JLabel("Each Price");
		lblNewLabel_3.setBounds(42, 124, 65, 14);
		contentPane.add(lblNewLabel_3);

		JLabel lblNewLabel_4 = new JLabel("Total Price");
		lblNewLabel_4.setBounds(220, 124, 86, 14);
		contentPane.add(lblNewLabel_4);

		JButton btnNewButton = new JButton("OK");
		btnNewButton.setForeground(Color.BLUE);
		btnNewButton.setFont(new Font("Tahoma", Font.BOLD, 12));
		btnNewButton.setBounds(330, 196, 78, 31);
		contentPane.add(btnNewButton);
		textField.setText(Login.user);
		textField_2.setText(String.valueOf((OrderBook.qty)));

		try {
			Connection con = DB.getConnection();
			PreparedStatement ps = con.prepareStatement("select BookName,BookPrice from addbook where bookid=?");
			ps.setInt(1, OrderBook.bookId);
			ResultSet rs = ps.executeQuery();
			rs.last();
			textField_1.setText(rs.getString(1));
			textField_3.setText(rs.getString(2));
			rs.beforeFirst();
			double price = Double.parseDouble(textField_3.getText()) * OrderBook.qty;
			textField_4.setText(String.valueOf(price));

			JLabel lblNewLabel_5 = new JLabel("Bill View");
			lblNewLabel_5.setFont(new Font("Tahoma", Font.BOLD, 16));
			lblNewLabel_5.setBounds(157, 13, 109, 31);
			contentPane.add(lblNewLabel_5);
			con.close();
		} catch (Exception e) {
			System.out.println(e);

		}

	}
}
