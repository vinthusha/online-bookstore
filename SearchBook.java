package bcas.assi.bookstore;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

public class SearchBook extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	static SearchBook frame;
	static int bookId;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					frame = new SearchBook();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public SearchBook() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBackground(Color.PINK);
		contentPane.setToolTipText("Technology");
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblNewLabel = new JLabel("Search Book");
		lblNewLabel.setBounds(150, 29, 136, 20);
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 16));
		contentPane.add(lblNewLabel);

		JLabel lblNewLabel_3 = new JLabel("Book Id");
		lblNewLabel_3.setForeground(Color.BLUE);
		lblNewLabel_3.setBounds(37, 68, 61, 20);
		contentPane.add(lblNewLabel_3);

		textField = new JTextField();
		textField.setBounds(95, 68, 71, 20);
		contentPane.add(textField);
		textField.setColumns(10);

		JButton btnNewButton = new JButton("Search");
		btnNewButton.setFont(new Font("Tahoma", Font.BOLD, 12));
		btnNewButton.setForeground(Color.BLUE);
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				bookId = Integer.parseInt(textField.getText());

				if (bookId > 0) {
					if (SearchDB.checkBook(bookId)) {
						View.main(new String[] {});
						// frame.dispose();

					} else {
						JOptionPane.showMessageDialog(SearchBook.this, " Sorry, Callno doesn't exist!");
					} // end of checkbook if-else

				} else {
					JOptionPane.showMessageDialog(SearchBook.this, "Sorry, unable to issue!");
				} // end of bookId if-else

			}
		});
		btnNewButton.setBounds(196, 66, 89, 23);
		contentPane.add(btnNewButton);

		JLabel lblNewLabel_4 = new JLabel("");
		Image img = new ImageIcon(this.getClass().getResource("/Books.png")).getImage();
		lblNewLabel_4.setIcon(new ImageIcon(img));
		lblNewLabel_4.setBounds(289, 69, 135, 170);
		contentPane.add(lblNewLabel_4);

		JButton btnNewButton_1 = new JButton("Back");
		btnNewButton_1.setFont(new Font("Tahoma", Font.BOLD, 12));
		btnNewButton_1.setForeground(Color.BLUE);
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Category.main(new String[] {});
			}
		});
		btnNewButton_1.setBounds(37, 208, 71, 31);
		contentPane.add(btnNewButton_1);

		JButton btnOrderBook = new JButton("Order Book");
		btnOrderBook.setForeground(Color.BLUE);
		btnOrderBook.setFont(new Font("Tahoma", Font.BOLD, 12));
		btnOrderBook.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				OrderBook.main(new String[] {});
			}
		});
		btnOrderBook.setBounds(131, 124, 112, 31);
		contentPane.add(btnOrderBook);
	}
}
