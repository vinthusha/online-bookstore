package bcas.assi.bookstore;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

public class CardDetail extends JFrame {

	private JPanel contentPane;
	private JTextField txtShoppingCard;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CardDetail frame = new CardDetail();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public CardDetail() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBackground(Color.PINK);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		txtShoppingCard = new JTextField();
		txtShoppingCard.setBounds(133, 77, 86, 20);
		contentPane.add(txtShoppingCard);
		txtShoppingCard.setColumns(10);

		JLabel lblNewLabel = new JLabel("Name on Card");
		lblNewLabel.setForeground(Color.BLUE);
		lblNewLabel.setBounds(42, 80, 81, 14);
		contentPane.add(lblNewLabel);

		JLabel lblNewLabel_1 = new JLabel(" Card Detail");
		lblNewLabel_1.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblNewLabel_1.setBounds(133, 22, 155, 20);
		contentPane.add(lblNewLabel_1);

		JLabel lblNewLabel_2 = new JLabel("Card No");
		lblNewLabel_2.setForeground(Color.BLUE);
		lblNewLabel_2.setBounds(42, 115, 46, 14);
		contentPane.add(lblNewLabel_2);

		JLabel lblNewLabel_3 = new JLabel("Expiration Date");
		lblNewLabel_3.setForeground(Color.BLUE);
		lblNewLabel_3.setBounds(42, 154, 81, 14);
		contentPane.add(lblNewLabel_3);

		JLabel lblNewLabel_4 = new JLabel("Security Code");
		lblNewLabel_4.setForeground(Color.BLUE);
		lblNewLabel_4.setBounds(42, 191, 81, 14);
		contentPane.add(lblNewLabel_4);

		textField = new JTextField();
		textField.setBounds(133, 112, 86, 20);
		contentPane.add(textField);
		textField.setColumns(10);

		textField_1 = new JTextField();
		textField_1.setBounds(133, 151, 86, 20);
		contentPane.add(textField_1);
		textField_1.setColumns(10);

		textField_2 = new JTextField();
		textField_2.setBounds(133, 188, 86, 20);
		contentPane.add(textField_2);
		textField_2.setColumns(10);

		JButton btnNewButton = new JButton("Confirm");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Bill.main(new String[] {});
			}
		});
		btnNewButton.setForeground(Color.BLUE);
		btnNewButton.setFont(new Font("Tahoma", Font.BOLD, 12));
		btnNewButton.setBounds(288, 208, 89, 31);
		contentPane.add(btnNewButton);

		JLabel lblNewLabel_5 = new JLabel("");
		Image img = new ImageIcon(this.getClass().getResource("/Shopping Card.png")).getImage();
		lblNewLabel_5.setIcon(new ImageIcon(img));
		lblNewLabel_5.setBounds(262, 63, 135, 128);
		contentPane.add(lblNewLabel_5);
	}

}
